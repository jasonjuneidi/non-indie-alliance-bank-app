import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('NonIndie_Alliance_App', () => App);
