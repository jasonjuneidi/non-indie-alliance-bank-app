'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ActivityIndicator,
    TouchableOpacity,
    TouchableHighlight,
    Dimensions,
    ListView,
    Image,
} from 'react-native';

import {
    Header,
    Left,
    Right,
    Body,
    Container,
    Button,
    Icon,
    Title,
    Subtitle,
    Content,
    Text,
} from 'native-base'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions'; //Import your actions
import styles from './styles'

import NavigationService from '../../navigation/navigationservice'
import ModalDropdown from 'react-native-modal-dropdown';

const renderTouchable = () => <Button transparent />;

class Login extends Component {
    constructor(props) {
        super(props);
        this.touchable = React.createRef()

        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }).cloneWithRows(['Current Account / -i', 'Basic Current Account / -i']),
        };
    }

    renderRow(data) {
        return (
            <Text>{`\u2022 ${data}`}</Text>
        );
    }

    componentDidMount() {
    }

    openMenu() {
        this.props.navigation.openDrawer()
    }

    simulatePress() {
        this.touchable.current.click();
    }

    render() {
        if (this.props.loading) {
            return (
                <View style={styles.activityIndicatorContainer}>
                    <ActivityIndicator animating={true} />
                </View>
            );
        } else {
            return (
                <Container>
                    <Header>
                        <Left>
                            <Button
                                transparent
                                onPress={this.openMenu.bind(this)}>
                                <Icon name='menu' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Welcome</Title>
                        </Body>
                        <Right>
                            <ModalDropdown options={['Edit Profile', 'Sign Out']}>
                                <View style={styles.dropdown}>
                                    <View>
                                        <Title
                                            style={styles.profileName}>Jason</Title>
                                        <Subtitle
                                            style={styles.profilePos}>Mobile App Developer</Subtitle>
                                    </View>
                                    <Icon name='arrow-dropdown'
                                        style={{ marginTop: 5, marginLeft: 20, color: 'white', fontSize: 20, }} />
                                </View>
                            </ModalDropdown>
                        </Right>
                    </Header>
                    <Content>
                        <Text>Hello, Full Name</Text>
                        <Text>What would you like to do today?</Text>
                        <View style={{ backgroundColor: 'black', height: 1 }} />
                        <Text>The Digital Banking One System (DBOS) currently caters to the following products. To apply for a product that is not listed here, please kindly visit any Alliance Bank branch.</Text>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                        />

                        <View
                            style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <View
                                style={{ flexDirection: 'column', justifyContent: 'center', borderWidth: 1 }}>
                                < TouchableOpacity
                                    style={{ width: 150, height: 150, }}
                                    onPress={() => NavigationService.navigate('CompanySearch')}>
                                    <Image
                                        style={styles.menuBtn}
                                        source={require('../../assets/businessbtn.png')} />
                                </TouchableOpacity>
                                <Button
                                    onPress={() => NavigationService.navigate('CompanySearch')}
                                    transparent>
                                    <Text>Business Account</Text>
                                </Button>
                            </View>
                        </View>
                    </Content >
                </Container >
            );
        }
    }
};

// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        // loading: state.dataReducer.loading,
        data: state.dataReducer.data
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(Login);