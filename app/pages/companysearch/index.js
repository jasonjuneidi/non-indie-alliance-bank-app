import React, { Component } from 'react'

import {
    StyleSheet,
    View,
    ActivityIndicator,
    TouchableOpacity,
    Dimensions,
    ListView,
    Modal
} from 'react-native';

import {
    Header,
    Left,
    Right,
    Body,
    Container,
    Button,
    Icon,
    Title,
    Subtitle,
    Content,
    Text,
    Form,
    Item,
    Picker,
    Grid,
    Row,
    Col,
    Input,
    CheckBox,
    ListItem
} from 'native-base'

import PopupDialog from 'react-native-popup-dialog'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions'; //Import your actions
import styles from './styles'
import ModalDropdown from 'react-native-modal-dropdown'

class CompanySearh extends Component {
    constructor(props) {
        super(props)
        this.state = ({
            selected2: 'key0',
            localcouncil: '',
            regsabah: false,
            result: false,
            tnc: false
        })
    }

    onValueChange2(value: string) {
        this.setState({
            selected2: value
        });
    }

    onTncChange() {
        if(this.state.tnc) {
            this.setState({
                tnc: false
            })
        }
        else if (!this.state.tnc) {
            this.setState({
                tnc: true
            })
        }
    }

    onRegSabah() {
        if (this.state.regsabah) {
            this.setState({
                regsabah: false
            })
        }
        else if (!this.state.regsabah) {
            this.setState({
                regsabah: true
            })
        }
    }

    scrolltoBottom() {
        this.component._root.scrollToEnd()
    }

    showResult() {
        this.setState({
            result: true
        })
        setTimeout(
            this.scrolltoBottom.bind(this), 1000
        )
    }

    resetResult() {
        this.setState({
            result: false
        })
    }

    openMenu() {
        this.props.navigation.openDrawer()
    }

    showdialog() {
        this.popupDialog.show()
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button
                            transparent
                            onPress={this.openMenu.bind(this)}>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Company Search</Title>
                    </Body>
                    <Right>
                        <ModalDropdown options={['Edit Profile', 'Sign Out']}>
                            <View style={styles.dropdown}>
                                <View>
                                    <Title
                                        style={styles.profileName}>Title here</Title>
                                    <Subtitle
                                        style={styles.profilePos}>Subtitle here</Subtitle>
                                </View>
                                <Icon name='arrow-dropdown' style={{ marginLeft: 5, color: 'white' }}></Icon>
                            </View>
                        </ModalDropdown>
                    </Right>
                </Header>
                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    height={0.7}
                    width={0.7}>
                    <View
                        style={{margin: 5}}>
                        <Title
                            style={{textAlign: 'left', color: 'blue'}}>
                            Confirm to proceed?
                        </Title>
                    </View>
                    <View style={{backgroundColor: 'blue', height: 1}}/>
                    <View
                        style={{margin: 5}}>
                        <Title
                            style={{textAlign: 'left', color: 'blue'}}>
                            By clicking Next, system will perform the relevant background checks on the below company. Do you confirm to proceed?
                        </Title>
                    </View>
                    <View
                        style={{ backgroundColor: '#D3D3D3', padding: 20 }}>
                        <Text
                            style={{ fontSize: 20, fontWeight: 'bold' }}>
                            Wing Cheong Sdn Bhd
                        </Text>
                        <Text
                            style={{ fontSize: 10 }}>
                            92117 - X {"\n"}27 June 2016
                        </Text>
                    </View>
                    <ListItem>
                            <CheckBox checked={this.state.onTncChange}
                                onPress={this.onTncChange.bind(this)} />
                            <Body>
                                <Text>Registered in Sabah / Sarawak</Text>
                            </Body>
                        </ListItem>
                </PopupDialog>
                <Content
                    ref={c => (this.component = c)}>
                    <Title
                        style={{ color: 'black', margin: 10 }}>Company Search</Title>
                    <Grid>
                        <Row
                            style={{ margin: 20 }}>
                            <Col>
                                <Text
                                    style={styles.boldText}>Entity Type*</Text>
                            </Col>
                            <Col>
                                <Form>
                                    <Item regular>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                                            // style={{ borderWidth: 1 }}
                                            placeholder="Select your SIM"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.selected2}
                                            onValueChange={this.onValueChange2.bind(this)}
                                        >
                                            <Picker.Item label="21 – Non-Financial Institution – Sole Proprietor" value="key0" />
                                            <Picker.Item label="24 -  Non-Financial Institution – Company" value="key1" />
                                            <Picker.Item label="51 – International Organisation" value="key2" />
                                            <Picker.Item label="91 - Others" value="key3" />
                                        </Picker>
                                    </Item>
                                </Form>
                            </Col>
                        </Row>
                        <Row
                            style={{ margin: 20 }}>
                            <Col>
                                <Text
                                    style={styles.boldText}>ID Type*</Text>
                            </Col>
                            <Col>
                                <Text>-</Text>
                            </Col>
                        </Row>
                        <Row
                            style={{ margin: 20 }}>
                            <Col>
                                <Text
                                    style={styles.boldText}>Company Name</Text>
                            </Col>
                            <Col>
                                <Item regular>
                                    <Input placeholder='Enter Company Name' />
                                </Item>
                            </Col>
                        </Row>
                        <ListItem>
                            <CheckBox checked={this.state.regsabah}
                                onPress={this.onRegSabah.bind(this)} />
                            <Body>
                                <Text>Registered in Sabah / Sarawak</Text>
                            </Body>
                        </ListItem>
                        <Row
                            style={{ margin: 20 }}>
                            <Col>
                                <Text
                                    style={styles.boldText}>SSM Registered Number*</Text>
                            </Col>
                            <Col>
                                <Item regular>
                                    <Input placeholder='Enter SSM Registration No.' />
                                </Item>
                            </Col>
                        </Row>

                        {this.state.regsabah ?
                            <Grid>
                                <Row
                                    style={{ margin: 20 }}>
                                    <Col>
                                        <Text
                                            style={styles.boldText}>Unique ID*</Text>
                                    </Col>
                                    <Col>
                                        <Item regular>
                                            <Input />
                                        </Item>
                                    </Col>
                                </Row>
                                <Row
                                    style={{ margin: 20 }}>
                                    <Col>
                                        <Text
                                            style={styles.boldText}>Local Council*</Text>
                                    </Col>
                                    <Col>
                                        <Form>
                                            <Item regular>
                                                <Picker
                                                    mode="dropdown"
                                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                    // style={{ borderWidth: 1 }}
                                                    placeholder="Select your SIM"
                                                    placeholderStyle={{ color: "#bfc6ea" }}
                                                    placeholderIconColor="#007aff"
                                                    selectedValue={this.state.selected2}
                                                    onValueChange={this.onValueChange2.bind(this)}
                                                >
                                                    <Picker.Item label="21 – Non-Financial Institution – Sole Proprietor" value="key0" />
                                                    <Picker.Item label="24 -  Non-Financial Institution – Company" value="key1" />
                                                    <Picker.Item label="51 – International Organisation" value="key2" />
                                                    <Picker.Item label="91 - Others" value="key3" />
                                                </Picker>
                                            </Item>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row
                                    style={{ margin: 20 }}>
                                    <Col>
                                        <Text
                                            style={styles.boldText}>Date of trading license used*</Text>
                                    </Col>
                                    <Col>
                                        <Form>
                                            <Item regular>
                                                <Picker
                                                    mode="dropdown"
                                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                    // style={{ borderWidth: 1 }}
                                                    placeholder="Select your SIM"
                                                    placeholderStyle={{ color: "#bfc6ea" }}
                                                    placeholderIconColor="#007aff"
                                                    selectedValue={this.state.selected2}
                                                    onValueChange={this.onValueChange2.bind(this)}
                                                >
                                                    <Picker.Item label="21 – Non-Financial Institution – Sole Proprietor" value="key0" />
                                                    <Picker.Item label="24 -  Non-Financial Institution – Company" value="key1" />
                                                    <Picker.Item label="51 – International Organisation" value="key2" />
                                                    <Picker.Item label="91 - Others" value="key3" />
                                                </Picker>
                                            </Item>
                                        </Form>
                                    </Col>
                                </Row>
                            </Grid>
                            : null}

                        <Row>
                            <Col></Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Button
                                        style={{ alignContent: 'flex-end', flex: 1, margin: 5 }}
                                        bordered>
                                        <Text>Back</Text>
                                    </Button>
                                    {this.state.result ?
                                        <Button
                                            onPress={this.resetResult.bind(this)}
                                            style={{ alignSelf: 'flex-end', flex: 1, margin: 5 }}>
                                            <Text>Reset</Text>
                                        </Button>
                                        :
                                        <Button
                                            onPress={this.showResult.bind(this)}
                                            style={{ alignSelf: 'flex-end', flex: 1, margin: 5 }}>
                                            <Text>Search</Text>
                                        </Button>
                                    }
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                    {this.state.result ?
                        <View
                            style={{ margin: 20 }}>
                            <Text>2 existing applications found</Text>
                            <View
                                style={{ backgroundColor: '#D3D3D3', padding: 20 }}>
                                <Text
                                    style={{ fontSize: 20, fontWeight: 'bold' }}>
                                    Wing Cheong Sdn Bhd
                            </Text>
                                <Text
                                    style={{ fontSize: 10 }}>
                                    92117 - X {"\n"}27 June 2016
                            </Text>
                                <View style={{ backgroundColor: 'black', height: 1 }} />
                                <View
                                    style={{ marginTop: 10 }}>
                                    <Grid>
                                        <Row
                                            style={{ marginBottom: 10 }}>
                                            <Icon name='alert'
                                                style={{ marginRight: 5, }} />
                                            <Col>
                                                <Title
                                                    style={{ textAlign: 'left', color: 'black', fontSize: 10 }}>
                                                    Existing Application Found
                                                </Title>
                                                <Subtitle
                                                    style={{ textAlign: 'left', color: 'black', fontSize: 10 }}>
                                                    Ref no: 123456
                                                </Subtitle>
                                            </Col>
                                            <Col>
                                                <Button
                                                    onPress={this.showdialog.bind(this)}
                                                    style={{ alignSelf: 'flex-end' }}>
                                                    <Text>Continue</Text>
                                                </Button>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Icon name='alert'
                                                style={{ marginRight: 5, }} />
                                            <Col>
                                                <Title
                                                    style={{ textAlign: 'left', color: 'black', fontSize: 10 }}>
                                                    Existing Application Found
                                                </Title>
                                                <Subtitle
                                                    style={{ textAlign: 'left', color: 'black', fontSize: 10 }}>
                                                    Ref no: 123456
                                                </Subtitle>
                                            </Col>
                                            <Col>
                                                <Button
                                                    style={{ alignSelf: 'flex-end' }}>
                                                    <Text>Continue</Text>
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </View>
                                <Button
                                    style={{ alignSelf: 'center', justifyContent: 'center' }}>
                                    <Text>Start a new application</Text>
                                </Button>
                            </View>
                        </View>
                        :
                        null
                    }
                </Content >
            </Container >
        )
    }
}

// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        // loading: state.dataReducer.loading,
        data: state.dataReducer.data
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(CompanySearh);