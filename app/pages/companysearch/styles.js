import {
    Dimensions
} from 'react-native'

export default {
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },

    whiteText: {
        color: 'white'
    },

    menuText: {
        color: 'white',
        marginRight: 10,
        padding: 10
    },

    profileName: {
        color: 'white',
        fontSize: 17
    },

    profilePos: {
        fontSize: 10
    },

    dropdown: {
        // width: 80,
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 5,
        margin: 10
    },

    menuBtn: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: 75,
        height: 50,
        // marginTop: 10,
        flex: 1,
        // padding: 10,
        margin: 20,
        // borderWidth: 1
    },

    boldText: {
        fontWeight: 'bold',
    }
}