export default {
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },

    row: {
        borderBottomWidth: 1,
        borderColor: "#ccc",
        padding: 10
    },

    title: {
        fontSize: 15,
        fontWeight: "600"
    },

    description: {
        marginTop: 5,
        fontSize: 14,
    },
    center: {
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    },

    backgroundImage: {
        flex: 1,
        // remove width and height to override fixed static size
        width: null,
        height: null,
        // alignItems: 'center',
        justifyContent: 'center',
    },

    button: {
        width: 200,
        justifyContent: 'center',
        marginLeft: 10,
    },

    loading: {
        // backgroundColor: 'rgba(52, 52, 52, 0.8)',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },

    input: {
        backgroundColor: 'rgb(255,255,255)',
        width: 200,
        height: 44,
        padding: 10,
        // borderWidth: 1,
        // borderColor: 'black',
        marginLeft: 10,
        marginBottom: 10,
    },

    whiteText: {
        color: 'white'
    }
}