'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    Text,
    ActivityIndicator,
    ImageBackground,
    TextInput,
    Alert
} from 'react-native';
import { Button } from 'native-base'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions'; //Import your actions
import styles from './styles'

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
    }

    login() {
        this.props.login();
    }

    render() {
        if (this.props.loading) {
            return (
                <View style={styles.activityIndicatorContainer}>
                    <ActivityIndicator animating={true} />
                </View>
            );
        } else {
            return (
                < ImageBackground source={require('../../assets/landing-img.jpg')} style={styles.backgroundImage} >
                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                        placeholder={'Username'}
                        placeholderTextColor={'#000'}
                        underlineColorAndroid={'#fff'}
                        style={styles.input}
                    />

                    <TextInput
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        placeholder={'Password'}
                        placeholderTextColor={'#000'}
                        underlineColorAndroid={'#fff'}
                        secureTextEntry={true}
                        style={styles.input}
                    />
                    {
                        this.state.clickable ?
                            <Button
                                rounded
                                block
                                style={styles.button}
                                onPress={this.login.bind(this)}>
                                <Text style={styles.whiteText}>Login</Text>
                            </Button>
                            :
                            <Button
                                // disabled
                                rounded
                                block
                                style={styles.button}
                                onPress={this.login.bind(this)}>
                                <Text style={styles.whiteText}>Login</Text>
                            </Button>
                    }

                    {this.state.isLoading ? <ActivityIndicator style={styles.loading} size='large' /> : null}

                    <Text>{this.state.responseText}</Text>
                </ImageBackground >
            );
        }
    }
};

// The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        // loading: state.dataReducer.loading,
        data: state.dataReducer.data
    }
}

// Doing this merges our actions into the component’s props,
// while wrapping them in dispatch() so that they immediately dispatch an Action.
// Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(Login);