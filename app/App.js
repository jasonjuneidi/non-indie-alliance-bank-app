/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store'; //Import the store

import { createStackNavigator, createDrawerNavigator } from 'react-navigation'

import NavigationService from './navigation/navigationservice'

import { Root } from 'native-base'

import SideBar from './pages/sidebar/sidebar'
import Login from './pages/login/index'
import Welcome from './pages/welcome/index'
import CompanySearch from './pages/companysearch';

export default () =>
    <Provider store={store}>
        <Root>
            <AppNavigator
                ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
        </Root>
    </Provider>


const Drawer = createDrawerNavigator(
    {
        Welcome: { screen: Welcome },
        CompanySearch: { screen: CompanySearch }
    },
    {
        initialRouteName: 'Welcome',
        contentComponent: props => <SideBar {...props} />
    }
)

const AppNavigator = createStackNavigator(
    {
        Login: { screen: Login },
        Drawer: { screen: Drawer },
    },
    {
        initialRouteName: 'Login',
        headerMode: 'null'
    }
)