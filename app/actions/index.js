export const DATA_AVAILABLE = 'DATA_AVAILABLE';

//Import the sample data
import Data from '../../sample.json';
import navigationservice from '../navigation/navigationservice';

import axios from 'axios'

export function getData() {
    return (dispatch) => {

        //Make API Call
        //For this example, I will be using the sample data in the json file
        //delay the retrieval [Sample reasons only]
        setTimeout(() => {
            const data = Data.instructions;
            dispatch({ type: DATA_AVAILABLE, data: data });
        }, 2000);

    };
}

export function login() {
    return (dispatch) => {
        // axios({
        //     method: 'post',
        //     url: 'https://apipay.hlb.com.my/hlb-cloud-login/auth/login',
        //     data: {
        //         username: '60123409597',
        //         userpass: '123456'
        //     },
        //     config: { headers: { 'Content-Type': 'application/json' } }
        // })
        //     .then(function (response) {
        //         navigationservice.navigate('Welcome')
        //         dispatch({ type: 'Login', data: response })
        //         console.log(response)
        //     })
        //     .catch(function (response) {
        //         console.log(response)
        //     })
        navigationservice.navigate('Drawer')
    }
}