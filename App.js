/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import AppRoot from './app/App'

export default class App extends Component {
  render() {
    return (
      <AppRoot />
    );
  }
}